extends Area2D

onready var kuyang = load("res://Scenes/Kuyang.tscn")
var init = false

func _on_Area2D2_body_entered(body):
	if body.get_name() == "Flax" and !init:
		init = true
		var node = kuyang.instance()
		node.position.x = position.x - rand_range(48, 96)
		node.position.y = position.y + rand_range(-48, 48)
		get_parent().add_child(node)
