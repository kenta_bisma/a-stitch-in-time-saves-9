extends Node

const LOCKS = [
	false,false,false,false
]

var tween = Tween.new()
var timer = Timer.new()

func _ready():
	Global.connect("passage", self, "passage")
	tween.pause_mode = PAUSE_MODE_PROCESS
	timer.pause_mode = PAUSE_MODE_PROCESS
		
func passage(num, node):
	if num == 0:
		if !LOCKS[num]:
			node.start("res://Data/text/premise.json")
			LOCKS[num] = true
	elif num == 1:
		node.start("res://Data/text/uroboros_loop.json")
	elif num == 3:
		if !LOCKS[num]:
			LOCKS[num] = true
		else:
			Global.life = 9
			Global.health = 100
			Global.checkpoint_pos = null
			node.start("res://Data/text/uroboros_loop.json")

func fade_out(target):
	tween.interpolate_property(target, "modulate:a",
		1,0,1,Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	tween.start()
	timer.set_one_shot(true)
	timer.start()

func fade_in(target):
	tween.interpolate_property(target, "modulate:a",
		0,1,1,Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	tween.start()
	timer.set_one_shot(true)
	timer.start()

func renew():
	tween = Tween.new()
	timer = Timer.new()
	tween.pause_mode = PAUSE_MODE_PROCESS
	timer.pause_mode = PAUSE_MODE_PROCESS
