extends Area2D

export (String) var sceneName = "Level 1"

#onready var player = get_parent().get_node("Player")

func _on_Area2D_body_entered(body):
	if body.get_name() == "Flax":
		Global.deal_void_damage()
		if Global.still_alive():
			get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
