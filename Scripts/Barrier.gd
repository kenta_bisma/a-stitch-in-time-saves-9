extends KinematicBody2D

export (int) var damage_dealt

var dead = false

onready var _animated_sprite = $AnimatedSprite
onready var _particles = $Particles2D
onready var _collision = $CollisionShape2D
onready var _rewindable = $Rewindable
onready var spool = load("res://Scenes/Spool.tscn")

func _ready():
	_rewindable.connect("finish_rewind", self, "_finish_rewind")

func _on_Area2D_body_entered(body):
	if body.get_name() == "Flax" and !dead:
		Global.deal_damage(damage_dealt)
		knockback(body)

func knockback(body):
	if self.position.x > body.position.x:
		body.velocity.x -= 250
	if self.position.x < body.position.x:
		body.velocity.x += 250
	body.velocity.y -= 50
	body.move_and_slide(body.velocity)

func _kill():
	$Death.play()
	_particles.emitting = true
	_rewindable.call_deferred("free")
	var node = spool.instance()
	node.position = self.position
	get_parent().add_child(node)
	Global.remove_retrace()
	_mark_dead()

func _mark_dead():
	self.dead = true
	_animated_sprite.visible = false
	_collision.disabled = true
	$Area2D/CollisionShape2D.disabled = true

func _finish_rewind():
	_kill()
