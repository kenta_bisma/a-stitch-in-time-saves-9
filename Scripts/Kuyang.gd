extends KinematicBody2D

export (int) var damage_dealt = 20

var move_speed = 80
var velocity = Vector2.ZERO

var dead = false

onready var target = get_parent().get_node("Flax")
onready var _particles = $Particles2D
onready var _collision = $CollisionShape2D
onready var _rewindable = $Rewindable
onready var spool = load("res://Scenes/Spool.tscn")

func _ready():
	_rewindable.connect("finish_rewind", self, "_finish_rewind")
		
func _physics_process(delta):
	if !dead:
		var vec = Vector2(target.position.x + rand_range(-64, 64), target.position.y + rand_range(-64, 64))
		velocity = (vec - position).normalized() * move_speed
		velocity = move_and_slide(velocity)

func _process(delta):
	if velocity.x > 0:
		$Sprite.flip_h = true
	else:
		$Sprite.flip_h = false
	if int(rand_range(0, 200)) >= 199 and !dead:
		$Idle.play()

func _on_Area2D_body_entered(body):
	if body.get_name() == "Flax" and !dead:
		Global.deal_damage(damage_dealt)
		knockback(body)

func knockback(body):
	if self.position.x > body.position.x:
		body.velocity.x -= 25
	if self.position.x < body.position.x:
		body.velocity.x += 25
	body.velocity.y -= 25
	body.move_and_slide(body.velocity)

func _on_Area2D_area_entered(area):
	if area.get_name() == "Retrace" and !dead:
		_kill()

func _kill():
	$Death.play()
	_particles.emitting = true
	_rewindable.call_deferred("free")
	var node = spool.instance()
	node.position = self.position
	get_parent().add_child(node)
	Global.remove_retrace()
	_mark_dead()

func _mark_dead():
	self.dead = true
	velocity = Vector2.ZERO
	$Idle.stop()
	$Sprite.visible = false
	_collision.disabled = true
	$Area2D/CollisionShape2D.disabled = true

func _finish_rewind():
	_kill()
