extends Area2D


export var content = ""
var dialogPath = ""

onready var dialogue = get_parent().get_node("CanvasLayer/TutorialDialogue")
# Called when the node enters the scene tree for the first time.
func _ready():
	dialogPath = "res://Data/text/tutorials/" + content + ".json"
	$Polygon2D.visible = true
	$Polygon2D/AnimationPlayer.play("hover")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if overlaps_body(get_parent().get_node("Flax")):
		if Input.is_action_just_pressed("interact") and !get_parent().get_node("Flax").is_reading:
			var text = getDialog()[0]
			$AudioStreamPlayer.play()
			dialogue.visible = true
			dialogue.get_node("NinePatchRect/MarginContainer/HBoxContainer/Title").text = text["Title"]
			dialogue.get_node("NinePatchRect/MarginContainer/HBoxContainer/Text").text = text["Text"]
			get_parent().get_node("Flax").is_reading = true
			get_tree().paused = true
			
		elif Input.is_action_just_pressed("ui_accept") and get_parent().get_node("Flax").is_reading:
				$AudioStreamPlayer.play()
				get_parent()
				dialogue.visible = false
				get_parent().get_node("Flax").is_reading = false
				get_tree().paused = false

func getDialog() -> Array:
	var f = File.new()
	assert(f.file_exists(dialogPath), "File path does not exist")
	
	f.open(dialogPath, File.READ)
	var json = f.get_as_text()
	
	var output = parse_json(json)
	
	if typeof(output) == TYPE_ARRAY:
		return output
	else:
		return []

func _on_Node2D_body_entered(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = false
		body.get_node("Label").visible = true


func _on_Node2D_body_exited(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = true
		body.get_node("Label").visible = false
