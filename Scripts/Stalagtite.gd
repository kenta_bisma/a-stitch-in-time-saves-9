extends KinematicBody2D

export (int) var dist

onready var _rewindable = $Rewindable
onready var _ray = $RayCast2D
onready var origin = position.y
onready var default_x = position.x

var done = false
var falling = false
var init = false
var _last_frame = 0

var velocity = Vector2.ZERO
var landed = false

var counter = 0

func _ready():
	_rewindable.connect("finish_rewind", self, "_finish_rewind")
	_rewindable.lockable = true
	

func _process(delta):
	position.x = default_x
	if init:
		if done:
			position.y = max(origin, position.y - 1)

func _physics_process(delta):
	if !init:
		var res = _ray.get_collider()
		if res != null:
			if res.name == "Flax":
				$AnimationPlayer.play("teeter")
				init = true
				_ray.collide_with_bodies = false
				collision_mask = res.collision_mask
				collision_layer = res.collision_layer
	if falling:
		_fall(delta)
	if is_on_floor():
		falling = false
		if !landed:
			print("loh")
			$Land.play()
			landed = true
	velocity = move_and_slide(velocity, Vector2.UP)

func knockback(body):
	if self.position.y > body.position.y:
		body.velocity.y -= 100
	if self.position.x >= body.position.x:
		body.velocity.x -= 500
	if self.position.x < body.position.x:
		body.velocity.x += 500
	body.move_and_slide(body.velocity)

func _fall(delta):
	velocity.y = 10000 * delta
	
func _finish_rewind():
	done = true
	$Area2D/CollisionShape2D.disabled = true
	_rewindable.call_deferred("free")

func _on_Area2D_body_entered(body):
	if body.name == "Flax" and falling:
		knockback(body)
		Global.deal_damage(2000)

func _start_fall():
	falling = true
