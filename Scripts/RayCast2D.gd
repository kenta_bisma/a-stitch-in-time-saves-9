extends RayCast2D


onready var _flax = get_parent()


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if Global.is_rewinding:
		var result = self.get_collider()
		if result != null:
			var rewindable = result.get_parent().get_node("Rewindable")
			if rewindable != null:
				rewindable.increment()
