extends Button

func _on_ReturnButton_pressed():
	Global.remove_retrace()
	if Global.checkpoint_pos != null:
		Flow.renew()
		add_child(Flow.tween)
		add_child(Flow.timer)
		Flow.fade_out(self.get_parent().get_parent())
		yield(Flow.timer, "timeout")
		get_tree().change_scene(str("res://Scenes/Main.tscn"))
	else:
		Flow.renew()
		add_child(Flow.tween)
		add_child(Flow.timer)
		Flow.fade_out(self.get_parent().get_parent())
		yield(Flow.timer, "timeout")
		Global.health = 100
		Global.life = 9
		get_tree().change_scene(str("res://Scenes/Loop Start.tscn"))

