extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Music.change("mixkit-nield-grohm-hanging-1-538 eugenio mininni.mp3")
	get_node("VBoxContainer/PlayButton").grab_focus()
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	Flow.fade_in(self)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
