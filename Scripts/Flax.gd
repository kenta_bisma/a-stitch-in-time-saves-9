extends KinematicBody2D

var speed = 100
export (int) var GRAVITY = 500
export (int) var jump_speed = -200

const UP = Vector2(0,-1)

var dir = 0
var velocity = Vector2()

onready var _animated_sprite = $AnimatedSprite

onready var _jump = $Jump
onready var _rewind = $Rewind
onready var _retrace = $Retrace
onready var _retraceback = $RetraceBack
onready var _walk = $Walk

onready var _particle = $Particles2D
onready var _ray = $RayCast2D

var animate_retrace = false
var is_reading = false

func _ready():
	Global.retrace = get_parent().get_node("Retrace")
	Global.connect("death", self, "death")

func get_input():
	dir = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		_jump.play()
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		_animated_sprite.flip_h = false
		dir += 1
	if Input.is_action_pressed('left'):
		_animated_sprite.flip_h = true
		dir -= 1
	if dir != 0:
		velocity.x = lerp(velocity.x, dir*speed, 0.25)
		_ray.rotation_degrees = 90 * -dir
	else:
		velocity.x = lerp(velocity.x, 0, 0.25)
	retrace()
	rewind()

func retrace():
	if Input.is_action_just_pressed('retrace') and !Global.has_retrace:
		_retrace.play()
		Global.place_retrace(self.position)
		_particle.emitting = true
		animate_retrace = true
	elif Input.is_action_just_pressed('retrace') and Global.has_retrace:
#		$Tween.interpolate_property(self, "position", self.position, Global.remove_retrace(), 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#		$Tween.start()
		_retrace.play()
		_retraceback.play()
		self.position = Global.remove_retrace()
		Global.deal_damage(5)
		_particle.emitting = true
		animate_retrace = true

func rewind():
	if Input.is_action_pressed("rewind") and dir == 0 and is_on_floor():
		if !_rewind.playing:
			_rewind.play()
		Global.is_rewinding = true
		_particle.emitting = true
	elif Input.is_action_just_released("rewind") or dir != 0 or !is_on_floor():
		_rewind.stop()
		Global.is_rewinding = false
		_particle.emitting = false

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func death():
	Music.change("game_over_transition.wav")
	Global.is_rewinding = false
	get_tree().paused = true
	_animated_sprite.play("dead")
			
func _process(delta):
	if animate_retrace:
		if velocity.y != 0:
			if !(_animated_sprite.animation == "retrace" and\
			_animated_sprite.frame <= _animated_sprite.frames.get_frame_count("retrace")-1):
				_animated_sprite.play("retrace_midair")
			else:
				var current_frame = _animated_sprite.frame
				_animated_sprite.animation = "retrace_midair"
				_animated_sprite.frame = current_frame
		else:
			if !(_animated_sprite.animation == "retrace_midair" and\
			_animated_sprite.frame <= _animated_sprite.frames.get_frame_count("retrace_midair")-1):
				_animated_sprite.play("retrace")
			else:
				var current_frame = _animated_sprite.frame
				_animated_sprite.animation = "retrace"
				_animated_sprite.frame = current_frame
			
		if _animated_sprite.frame == _animated_sprite.frames.get_frame_count(_animated_sprite.animation)-1:
			_particle.emitting = false
			animate_retrace = false
	elif velocity.y != 0:
		_walk.stop()
		_animated_sprite.play("jump")
	elif dir != 0:
		_animated_sprite.play("run")
		if !_walk.playing:
			_walk.play()
		if dir > 0:
			_animated_sprite.flip_h = false
		else:
			_animated_sprite.flip_h = true
	elif dir == 0 and Input.is_action_pressed("rewind"):
		_animated_sprite.play("rewind")
	elif dir == 0:
		_walk.stop()
		_animated_sprite.play("idle")
