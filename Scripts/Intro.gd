extends TextureRect

export var dialogPath = ""
export(float) var textSpeed = 0.05
 
var dialog
 
var phraseNum = 0
var finished = false
var tween = false

func _ready():
	dialog = get_dialogue()
	
	if dialogPath == "res://Data/text/cutscenes/intro.json":
		Music.change("intro.wav")
	else:
		Music.change("outro.wav")
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	$Timer.wait_time = textSpeed
	
	Flow.fade_in(self.get_parent())
	yield(Flow.timer, "timeout")
	
	assert(dialog, "Dialog not found")
	next_phrase()
 
func _process(_delta):
	$Indicator.visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		if finished:
			next_phrase()
		else:
			$Text.visible_characters = len($Text.text)
		$AudioStreamPlayer.play()
 
func get_dialogue() -> Array:
	var f = File.new()
	assert(f.file_exists(dialogPath), "File path does not exist")
	
	f.open(dialogPath, File.READ)
	var json = f.get_as_text()
	
	var output = parse_json(json)
	
	if typeof(output) == TYPE_ARRAY:
		return output
	else:
		return []
 
func next_phrase() -> void:
	if phraseNum >= len(dialog):
		Flow.fade_out(self.get_parent())
		yield(Flow.timer, "timeout")
		get_tree().change_scene(str("res://Scenes/Loop Start.tscn"))
		if dialogPath == "res://Data/text/cutscenes/Outro.json":
			Global.passage(3, null)
		queue_free()
		return
	
	finished = false
	
	$Text.bbcode_text = dialog[phraseNum]["Text"]
	
	$Text.visible_characters = 0
	
	Flow.fade_out($Sprite)
	yield(Flow.timer, "timeout")
	
	var f = File.new()
	var img = "res://Assets/sprites/" + dialog[phraseNum]["Name"] + ".png"

	$Sprite.texture = load(img)
	
	Flow.fade_in($Sprite)
	yield(Flow.timer, "timeout")
	
	while $Text.visible_characters < len($Text.text):
		$Text.visible_characters += 1
		$AudioStreamPlayer.play()
		$Timer.start()
		yield($Timer, "timeout")
	
	finished = true
	
	phraseNum += 1
	return
