extends KinematicBody2D

export (int) var damage_dealt

var move_speed = 80
export (NodePath) var patrol_path
var patrol_points
var patrol_index = 0
var velocity = Vector2.ZERO

var dead = false

onready var _animated_sprite = $AnimatedSprite
onready var _particles = $Particles2D
onready var _collision = $CollisionShape2D
onready var _rewindable = $Rewindable
onready var spool = load("res://Scenes/Spool.tscn")

func _ready():
	_rewindable.connect("finish_rewind", self, "_finish_rewind")
	if patrol_path:
		patrol_points = get_node(patrol_path).curve.get_baked_points()
	$Idle.play()
		
func _physics_process(delta):
	if !patrol_path or dead:
		return
	var target = patrol_points[patrol_index]
	if position.distance_to(target) < 1:
		patrol_index = wrapi(patrol_index + 1, 0, patrol_points.size())
		target = patrol_points[patrol_index]
	velocity = (target - position).normalized() * move_speed
	velocity = move_and_slide(velocity)

func _process(delta):
	if velocity.x > 0:
		_animated_sprite.flip_h = false
	else:
		_animated_sprite.flip_h = true

func _on_Area2D_body_entered(body):
	if body.get_name() == "Flax" and !dead:
		Global.deal_damage(damage_dealt)
		knockback(body)

func knockback(body):
	if self.position.x > body.position.x:
		body.velocity.x -= 500
	if self.position.x < body.position.x:
		body.velocity.x += 500
	body.velocity.y -= 100
	body.move_and_slide(body.velocity)

func _on_Area2D_area_entered(area):
	if area.get_name() == "Retrace" and !dead:
		_kill()

func _kill():
	$Death.play()
	$Idle.stop()
	_particles.emitting = true
	_rewindable.call_deferred("free")
	var node = spool.instance()
	node.position = self.position
	get_parent().add_child(node)
	Global.remove_retrace()
	_mark_dead()

func _mark_dead():
	self.dead = true
	velocity = Vector2.ZERO
	_animated_sprite.visible = false
	_collision.disabled = true
	$Area2D/CollisionShape2D.disabled = true

func _finish_rewind():
	_kill()
