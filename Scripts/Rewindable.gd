extends Node

export var rewind_process = 0

signal finish_rewind()

var lockable = false
var locked = false

onready var	_progress = $TextureProgress

func get_frame(numOfFrames):
	return ceil(float(rewind_process)/100*numOfFrames)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func increment():
	if (rewind_process == 99):
		emit_signal("finish_rewind")
		if lockable:
			locked = true
	rewind_process = min(100, rewind_process + 2)
	_progress.value = rewind_process
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _process(delta):
	if rewind_process == 0:
		_progress.visible = false
	else:
		_progress.visible = true
	if !locked:
		rewind_process = max(0, rewind_process - 1)
		_progress.value = rewind_process
