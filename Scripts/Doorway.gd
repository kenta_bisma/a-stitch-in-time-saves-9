extends Area2D

signal doorway_entered()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	if overlaps_body(get_parent().get_node("Flax")):
		if Input.is_action_just_pressed("interact"):
			get_tree().paused = true
			emit_signal("doorway_entered")

func _on_Doorway_body_entered(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = false
		body.get_node("Label").visible = true


func _on_Doorway_body_exited(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = true
		body.get_node("Label").visible = false
