extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.checkpoint_pos != null:
		$Flax.position = Global.checkpoint_pos
		Global.health = Global.checkpoint_health
		Global.life = Global.checkpoint_life
		Global.checkpoint_health = 0
		Global.checkpoint_life = 0
		Global.checkpoint_pos = null
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	Flow.fade_in(self)
	yield(Flow.timer, "timeout")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
