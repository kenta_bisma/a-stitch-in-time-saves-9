extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal passage(num, node)
signal health_update(health)
signal life_update(lives)
signal death()

var life = 9
var health = 100

var checkpoint_pos = null
var checkpoint_health = 0
var checkpoint_life = 0

var has_retrace = false
var retrace

var is_rewinding = false

func _ready():
	emit_signal("life_update", life)
	emit_signal("health_update", health)
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)

func _process(delta):
	emit_signal("life_update", life)
	emit_signal("health_update", health)
	if is_rewinding:
		deal_damage(1)

func deal_damage(num):
	health -= num
	while health < 0 and life >= 0:
		life -= 1
		emit_signal("life_update", life)
		health += 100
	if life < 0:
		emit_signal("death")
		Flow.fade_out(get_tree().current_scene)
		yield(Flow.timer, "timeout")
		remove_retrace()
		get_tree().paused = false
		get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
		return	
	emit_signal("health_update", health)

func heal(num):
	health = min(health + num, 100)
	emit_signal("health_update", health)

func remove_retrace():
	has_retrace = false
	retrace.visible = false
	var pos = retrace.position
	retrace.get_node("CollisionShape2D").disabled = true
	return pos
	
func place_retrace(pos):
	retrace.get_node("CollisionShape2D").disabled = false
	retrace.position = pos
	retrace.visible = true
	has_retrace = true

func deal_void_damage():
	remove_retrace()
	deal_damage(int(rand_range(20, 60)))

func still_alive():
	return life >= 0

func passage(num, node):
	emit_signal("passage", num, node)
	
