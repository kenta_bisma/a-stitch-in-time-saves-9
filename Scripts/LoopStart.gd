extends Node

var loop = false

func _ready():
	Music.change("ambience.wav")
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	Flow.fade_in(self)
	yield(Flow.timer, "timeout")
	
	Global.passage(0, $CanvasLayer/NPCDialogue/Box)
	Global.passage(3, $CanvasLayer/NPCDialogue/Box)
	$Doorway.connect("doorway_entered", self, "_doorway_entered")

func _doorway_entered():
	Flow.fade_out(self)
	yield(Flow.timer, "timeout")
	get_tree().paused = false
	Global.passage(2, null)
	get_tree().change_scene(str("res://Scenes/Main.tscn"))

