extends Area2D

export var id = ""

func _ready():
	$Polygon2D.visible = true
	$Polygon2D/AnimationPlayer.play("hover")

func _process(delta):
	if overlaps_body(get_parent().get_node("Flax")):
		if Input.is_action_just_pressed("interact"):
			get_parent().get_node("Flax").position = get_parent().get_node(id).position
			

func _on_Entanglement_body_entered(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = false
		body.get_node("Label").visible = true

func _on_Entanglement_body_exited(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = true
		body.get_node("Label").visible = false
