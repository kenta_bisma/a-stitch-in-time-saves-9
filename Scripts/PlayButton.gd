extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_PlayButton_pressed():
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	Flow.fade_out(self.get_parent().get_parent())
	yield(Flow.timer, "timeout")
	get_tree().change_scene(str("res://Scenes/Intro.tscn"))
