extends Control

onready var health_bar = $HealthBar/TextureProgress
onready var life = $Lives

func _ready():
	Global.connect("health_update", self, "_on_health_updated")
	Global.connect("life_update", self, "_on_life_updated")
	_on_health_updated(Global.health)
	_on_life_updated(Global.life)

func _on_health_updated(health):
	health_bar.value = health

func _on_life_updated(lives):
	var children = life.get_children()
	var i = 9-lives
	for child in children:
		if i > 0:
			child.frame = 1
			i -= 1
		else:
			child.frame = 0
