extends Button

func _on_MenuButton_pressed():
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	Flow.fade_out(self.get_parent().get_parent())
	yield(Flow.timer, "timeout")
	get_tree().change_scene(str("res://Scenes/Menu.tscn"))
