extends Area2D

export var content = ""
var dialogPath = ""

onready var dialogue = get_parent().get_node("CanvasLayer/NPCDialogue/Box")
# Called when the node enters the scene tree for the first time.
func _ready():
	dialogPath = "res://Data/text/" + content + ".json"
	$Polygon2D.visible = true
	$Polygon2D/AnimationPlayer.play("hover")

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if overlaps_body(get_parent().get_node("Flax")):
		if Input.is_action_just_pressed("interact") and !get_parent().get_node("Flax").is_reading:
			dialogue.start(dialogPath)

func _on_Node2D_body_entered(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = false
		body.get_node("Label").visible = true


func _on_Node2D_body_exited(body):
	if body.get_name() == "Flax":
		$Polygon2D.visible = true
		body.get_node("Label").visible = false
