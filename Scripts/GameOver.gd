extends Node

func _ready():
	Music.change("outro.wav")
	get_node("VBoxContainer/ReturnButton").grab_focus()
	Flow.renew()
	add_child(Flow.tween)
	add_child(Flow.timer)
	Flow.fade_in(self)
	yield(Flow.timer, "timeout")

func _unhandled_input(event):
	print(event)

