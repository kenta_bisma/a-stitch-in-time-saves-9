extends Control

export var dialogPath = ""

export(float) var textSpeed = 0.05
var dialog
var phraseNum = 0
var finished = false
var done = false

func _ready():
	$Timer.wait_time = textSpeed
	
func start(path):
	get_parent().visible = true
	get_parent().get_parent().get_parent().get_node("Flax").is_reading = true
	get_tree().paused = true
	done = false
	dialogPath = path
	dialog = get_dialogue()
	assert(dialog, "Dialog not found")
	next_phrase()
 
func _process(_delta):
	if Input.is_action_just_pressed("ui_accept"):
		if done:
			phraseNum = 0
			get_parent().visible = false
			get_parent().get_parent().get_parent().get_node("Flax").is_reading = false
			get_tree().paused = false
		elif finished:
			next_phrase()
		else:
			$Text.visible_characters = len($Text.text)
		if $Indicator.visible:
			$AudioStreamPlayer.play()
	$Indicator.visible = finished
 
func get_dialogue() -> Array:
	var f = File.new()
	assert(f.file_exists(dialogPath), "File path does not exist")
	
	f.open(dialogPath, File.READ)
	var json = f.get_as_text()
	
	var output = parse_json(json)
	
	if typeof(output) == TYPE_ARRAY:
		return output
	else:
		return []
 
func next_phrase() -> void:
	if phraseNum >= len(dialog):
		done = true

		phraseNum = 0
		if dialogPath == "res://Data/text/premise.json":
			Global.passage(1, self)
		return
	
	finished = false
	
	$Text.bbcode_text = dialog[phraseNum]["Text"]
	get_parent().get_node("NinePatchRect/Title").bbcode_text = str(dialog[phraseNum]["Char"]).capitalize()
	
	$Text.visible_characters = 0
	
	var f = File.new()
	var img = "res://Assets/sprites/" + dialog[phraseNum]["Char"] + "_" + dialog[phraseNum]["Mood"] + ".png"
	$Sprite.texture = load(img)
	
	while $Text.visible_characters < len($Text.text):
		$Text.visible_characters += 1
		$AudioStreamPlayer.play()
		$Timer.start()
		yield($Timer, "timeout")
	
	finished = true
	
	phraseNum += 1
	return
