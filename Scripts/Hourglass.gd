extends Area2D

onready var _animated_sprite = $AnimatedSprite
onready var _rewindable = $Rewindable

var done = false
var _last_frame = 0
var playing = false

func _ready():
	_rewindable.connect("finish_rewind", self, "_finish_rewind")
	_rewindable.lockable = true
	_animated_sprite.frame = 0

func _process(delta):
	if !done:
		if !playing and _rewindable.rewind_process > 0:
			$Audio.play()
			playing = true
		_last_frame = _rewindable.get_frame(_animated_sprite.frames.get_frame_count("default"))
		_animated_sprite.frame = _last_frame
	else:
		_animated_sprite.play("fixed")

func _finish_rewind():
	done = true
	$Area2D/CollisionShape2D.disabled = true
	_rewindable.call_deferred("free")
	Global.checkpoint_pos = self.position
	Global.checkpoint_health = Global.health
	Global.checkpoint_life = Global.life
