extends RigidBody2D


func _on_Area2D_body_entered(body):
	if body.get_name() == "Flax":
		Global.heal(int(rand_range(10,20)))
		queue_free()
